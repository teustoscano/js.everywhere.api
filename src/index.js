// index.js
// This is the main entry point of our application
const helmet = require('helmet')
const cors = require('cors')
const express = require('express')
const app = express()
app.use(helmet())
app.use(cors())
const { ApolloServer } = require('apollo-server-express')
const jwt = require('jsonwebtoken')
const typeDefs = require('./schema')
require('dotenv').config()
const db = require('./db')
const models = require('./models')
const resolvers = require('./resolvers')
const depthLimit = require('graphql-depth-limit')
const { createComplexityLimitRule} = require('graphql-validation-complexity')

const port = process.env.PORT || 4000
const DB_HOST = process.env.DB_HOST

db.connect(DB_HOST)

const getUser = token => {
    if (token) {
        try {
            return jwt.verify(token, process.env.JWT_SECRET)
        } catch (err) {
            throw new Error('Session Invalid')
        }
    }
}

const server = new ApolloServer({
    typeDefs, resolvers, validationRules: [depthLimit(5), createComplexityLimitRule(1000)], context: async ({req}) => {
        const token = req.headers.authorization
        const user = await getUser(token)
        console.log(user)
        return { models, user }
    }
})
server.applyMiddleware({ app, path: '/api' })

app.listen({ port }, () => {
    console.log(`GraphQL server running at ${port}${server.graphqlPath}`)
})