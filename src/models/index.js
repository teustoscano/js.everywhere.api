const Note = require('./note')
const User = require('./user')
const { model } = require('mongoose')

const models = {
    Note,
    User
}

module.exports = models